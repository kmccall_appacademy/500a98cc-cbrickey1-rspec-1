def echo (something)
  something
end


def shout(something)
  something.upcase
end


def repeat(something, repeats = 2)
  final_string = String.new
  repeats.times { final_string += "#{something} "}
  final_string.chop
end


def start_of_word(word, n)
  word[0...n]
end


def first_word(word)
  word_array = word.split(/\s+/)
  word_array[0]
end


def titleize(title)
  word_array = title.split(/\s+/)
  word_handler(word_array).chop
end


def word_handler(word, idx)
  little_words = ["and", "the", "over"]

  if (idx == 0) || (little_words.include?(word) == false)
    return word.capitalize
  else
    return word
  end

end


def titleize(title)
  word_array = title.split(/\s+/)
  titleized_array = Array.new
  word_array.each_with_index { |word, idx| titleized_array << word_handler(word, idx) }
  titleized_array.join(" ")
end
