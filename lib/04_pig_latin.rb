#This collection of methods DOES NOT handle words that only contain consonants (inclusive of 'qu')


def vowel?(word, idx)
  return true if ["a", "e", "i", "o", "u"].include?(word.downcase[idx])
  false
end


def consonant_break(word)

  1.upto(word.length - 1) do |idx|
    if (word[idx].downcase == "u") && (word[idx - 1].downcase == "q")
      next
    elsif vowel?(word, idx)
      return idx
    end
  end

  -1
end


def capitalized_indices(word)
  indices_array = Array.new

  0.upto(word.length - 1) do |idx|
    letter = word[idx]
    indices_array << idx if letter == letter.upcase
  end

  indices_array
end


def pig_latin_translate(word)

  translated_word =
    if vowel?(word, 0)
      "#{word.downcase}ay"
    else
      break_index = consonant_break(word)
      front = word[(break_index)..-1].downcase
      back = word[0..(break_index - 1)].downcase
      "#{front}#{back}ay"
    end

    capitalized_indices(word).each do |idx|
      translated_word[idx] = translated_word[idx].upcase
    end

  translated_word
end


def translate (english_string)
  english_array = english_string.split(/\s+/)
  translated_array = Array.new
  english_array.each { |word| translated_array << pig_latin_translate(word) }
  translated_array.join(" ")
end
