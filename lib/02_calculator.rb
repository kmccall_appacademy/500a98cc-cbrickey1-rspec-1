def add(a, b)
  a + b
end


def subtract(a, b)
  a - b
end


def sum(arr)
  sum = 0
  arr.each { |num| sum += num }
  sum
end


def multiply(*number)
  product = 1
  number.each { |num| product *= num }
  product
end


def power(a, b)
  a ** b
end


def factorial(n)
  return 1 if n == 0
  (1..n).inject { |product, n| product * n }
end
